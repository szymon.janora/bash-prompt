# Bash Prompt

![Screenshot](Doc/Screenshot.png)

## Installation

Download the Bash script

    curl https://gitlab.com/szymon.janora/bash-prompt/-/raw/master/bash_prompt.sh -o ~/.bash_prompt

And source it in your `.bashrc`

    source ~/.bash_prompt

#!/usr/bin/env bash

# Bash Prompt
# Copyright (C) 2020  Szymon Janora
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


function cmd_return() {
    RETVAL=$?
    echo "$RETVAL"
}

HORIZONTAL_LINE_PS_="\342\224\200"
DOWN_RIGHT_VERTICAL_LINE_PS_="\342\224\214"
UP_RIGHT_VERTICAL_LINE_PS_="\342\224\224"
LEFT_UP_VERTICAL_LINE_PS_="\342\224\230"
DATE_PS_="\[\e[33m\]\d\[\e[m\]"
TIME_PS_="\[\e[31m\]\t\[\e[m\]"
USER_AND_HOST_PS_="\[\e[32m\]\u\[\e[m\]\[\e[31m\]@\[\e[m\]\[\e[36m\]\h\[\e[m\]"
WORK_DIR_PS_="\[\e[34m\]\w\[\e[m\]"
CMD_STATUS_="\[\e[31m\]?=\[\e[m\]\[\e[35m\]\`cmd_return\`\[\e[m\]"
HL_PS_="${HORIZONTAL_LINE_PS_}${HORIZONTAL_LINE_PS_}${HORIZONTAL_LINE_PS_}"
BEFORE_CMD_STATUS_PS_="${HL_PS_}[${CMD_STATUS_}]${HL_PS_}${LEFT_UP_VERTICAL_LINE_PS_}"
FIRST_LINE_PS_="${DOWN_RIGHT_VERTICAL_LINE_PS_}${HL_PS_}[${DATE_PS_}]${HL_PS_}[${TIME_PS_}]${HL_PS_}[${USER_AND_HOST_PS_}]${HL_PS_}[${WORK_DIR_PS_}]${BEFORE_CMD_STATUS_PS_}"
SECOND_LINE_PS_="\n${UP_RIGHT_VERTICAL_LINE_PS_}${HORIZONTAL_LINE_PS_}>\\$"

function top_line_up() {
    PROMPT_FIRST_LINE_="${FIRST_LINE_PS_@P}"
    echo "${#PROMPT_FIRST_LINE_}"
    # Perhaps, You can fill line to right:
    # https://stackoverflow.com/questions/10751867/utf-8-width-display-issue-of-chinese-characters
    # https://stackoverflow.com/questions/35052500/print-to-the-end-of-terminal
}

export PS1="${FIRST_LINE_PS_}${SECOND_LINE_PS_}"
